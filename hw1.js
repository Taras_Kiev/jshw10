"use strict";


// Завдання практичне

document.querySelector(".tabs").addEventListener("click", (event)=>{
   document.querySelector(".active").classList.remove("active")
    event.target.classList.add("active")

    document.querySelector(".show").classList.remove("show")
    console.log(event.target.getAttribute("data-tab"))
    const dataAtr = event.target.getAttribute("data-tab");
   document.querySelector(`.tabs-content[data-tab=${dataAtr}]`).classList.add("show");
})

